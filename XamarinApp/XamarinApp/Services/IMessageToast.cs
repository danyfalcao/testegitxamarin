﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinApp.Services
{
    public interface IMessageToast
    {
        void LongAlert(string message);
        void ShortAlert(string message);
    }
}

﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinApp.Services
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}

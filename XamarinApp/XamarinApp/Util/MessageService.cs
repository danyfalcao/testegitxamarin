﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using XamarinApp.Services;

namespace XamarinApp.Util
{
    public class MessageService : IMessageService
    {
        public async Task ShowAsync(string message)
        {
            await XamarinApp.App.Current.MainPage.DisplayAlert("OI", message, "OK");
        }

        public async Task<bool> ShowAlertConfirmationAsync(string message)
        {
            return await XamarinApp.App.Current.MainPage.DisplayAlert("OI", message, "Sim", "Não");
        }

        public MessageService()
        { }
    }
}

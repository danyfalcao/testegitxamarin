﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace XamarinApp.ViewModels
{
    class MainPageViewModel : BaseViewModel
    {
        public string _mensagem;

        public string Mensagem
        {
            get { return _mensagem; }
            set { SetProperty(ref _mensagem, value); }
        }

        public ICommand MensagemCommand { get; set; }
        public ICommand LimparCommand { get; set; }
        public ICommand NovoCommand { get; set; }
        public object MessageToast { get; private set; }

        public MainPageViewModel()
        {
            MensagemCommand = new Command(MostrarMensagem);
            LimparCommand = new Command(Limpar);
            NovoCommand = new Command(Novo);
        }

        private  void MostrarMensagem()
        {
            Mensagem = "Mensagem Apresentada com Sucesso";
            //ToastService.ShortAlert("OK! Tudo Certinho...");
            MessageService.ShowAsync("OK! Tudo Certinho...");
        }

        private  void Limpar()
        {
            Mensagem = "Clique no botão para ver a mensagem"; ;
        }

        private void Novo()
        {
            NavigationService.NavigateToAsync<NovaPaginaViewModel>();
        }
    }
}

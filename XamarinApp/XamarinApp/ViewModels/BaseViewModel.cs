﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XamarinApp.Services;
using XamarinApp.Util;

namespace XamarinApp.ViewModels
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        protected IMessageToast ToastService { get; set; }
        protected ICommand ItemSelecionadoCommand { get; set; }
        protected IMessageService MessageService { get; set; }
        protected INavigationService NavigationService { get; set; }
        protected  Page CurrentPage { get; private set; }

        protected virtual void CurrentPageOnAppearig(object sender, EventArgs eventArgs) { }
        protected virtual void CurrentPageOnDisappearin(object sender,EventArgs eventArgs) {  }


        protected BaseViewModel()
        {
            ToastService = DependencyService.Get<IMessageToast>();
            MessageService = new MessageService();
            NavigationService = new NavigationService();
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName]string propertyName =null)
        {
            if(EqualityComparer<T>.Default.Equals(storage,value))
            {
                return false;
            }
            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        public void Initialize(Page page)
        {
            CurrentPage = page;
            CurrentPage.Appearing += CurrentPageOnAppearig;  //Página Abre
            CurrentPage.Disappearing += CurrentPageOnDisappearin; // Página fecha
        }

        public virtual Task InitializeAsync(object parameter)
        {
            return Task.FromResult(false);
        }

    }
}

﻿using Refit;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;
using XamarinApp.Models;
using XamarinApp.Services;
using XamarinApp.Util;

namespace XamarinApp.ViewModels
{
    public class NovaPaginaViewModel :BaseViewModel
    {
        public NovaPaginaViewModel()
        {
            ItemSelecionadoCommand = new Command(SelecionarItem);
        }

        private ObservableCollection<User> usuarios;
        public ObservableCollection<User> Usuarios
        {
            get { return usuarios; }
            set { SetProperty(ref usuarios, value); }
        }

        private User _itemSelecionado;
        public User ItemSelecionado
        {
            get { return _itemSelecionado; }
            set { SetProperty (ref _itemSelecionado, value); }
        }
        protected override void CurrentPageOnAppearig(object sender, EventArgs eventArgs)
        {
            CarregarUser();
            ToastService.ShortAlert("Voltei...");
        }

        protected override void CurrentPageOnDisappearin(object sender, EventArgs eventArgs)
        {
            ToastService.ShortAlert("Já Fui...");
        }

        public async void CarregarUser()
        {
            var url = "https://jsonplaceholder.typicode.com";
            var retorno = await RestService.For<IRestAPI>(url).SelecionarTodos();
            Usuarios = new ObservableCollection<User>();

            foreach (var user in retorno)
            {
                Usuarios.Add(user);

            }
        }

        public void SelecionarItem()
        {
            var banco = Contexto.GetInstance().dataBase;
            banco.Insert(ItemSelecionado);

            var novo = banco.Table<User>().FirstOrDefault(c => c.id == ItemSelecionado.id);
            MessageService.ShowAsync("Item Adcionado no Banco:" + novo.title);
        }
    }
}
